# Kata template for Java using IntelliJ and Maven

Facilitators should fork this template project to create a **Java** starter project which uses **IntelliJ** and **Maven** to develop a solution for a given kata.

First, ensure that a sub-group https://gitlab.com/boston-software-crafters/kata-starter-projects/kata-name exists where *kata-name* is name of the kata being set up, creating it as necessary.

Next, ensure that a sub-group https://gitlab.com/boston-software-crafters/kata-starter-projects/kata-name/java exists, creating it as necessary.

Then fork the new project to https://gitlab.com/boston-software-crafters/kata-starter-projects/kata-name/java/ and tailor the forked starter files as necessary (depends on the kata).

After forking, replace the content of this file (**README.md**) with the content of **STARTER_README.md** and then delete the **STARTER_README.md** file.

Edit this new **README.md** with additional instructions as appropriate in all sections. It is highly recommended that abstract terms and names be made concrete.

The facilitator should do at least one fork of the starter project to create a project for the team to use during the scheduled event.

Once the starter project is forked, it must then be cloned to the facilitator's computer where the event attendees will take turns controlling the computer running the kata.
